-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table inventaris.barang
CREATE TABLE IF NOT EXISTS `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `jenis` int(11) NOT NULL,
  `merk` varchar(100) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang: ~11 rows (approximately)
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` (`id`, `code`, `name`, `jenis`, `merk`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(3, 'A.1', 'Monitor LCD 21', 1, 'Samsung', 1, '2017-04-29 11:46:33', 1, '2017-06-06 21:05:58'),
	(4, 'A.2', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-04-29 11:47:12', 1, '2017-06-06 21:06:05'),
	(5, 'B.1', 'Toyota Avanza', 5, 'Toyota', 1, '2017-04-29 11:47:32', 1, '2017-06-06 21:06:14'),
	(6, 'A.3', 'Laptop HP ZBook', 1, 'HP', 1, '2017-06-10 20:56:11', 1, '2017-06-10 20:56:11'),
	(7, 'A.3', 'Laptop HP ZBook', 1, 'HP', 1, '2017-06-10 20:56:39', 1, '2017-06-10 20:56:39'),
	(8, 'A.4', 'Mouse Logitech', 1, 'Logitech', 1, '2017-06-10 21:01:07', 1, '2017-06-10 21:01:07'),
	(9, 'A.5', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-06-10 21:01:43', 1, '2017-06-10 21:01:43'),
	(10, 'A.5', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-06-10 21:02:24', 1, '2017-06-10 21:02:24'),
	(11, 'A.5', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-06-12 02:07:30', 1, '2017-06-12 02:07:30'),
	(12, 'A.5', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-06-12 02:07:34', 1, '2017-06-12 02:07:34'),
	(13, 'A.5', 'Keyboard Logitech', 1, 'Logitech', 1, '2017-06-12 02:07:45', 1, '2017-06-12 02:07:45');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_beli
CREATE TABLE IF NOT EXISTS `barang_beli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_beli: ~8 rows (approximately)
/*!40000 ALTER TABLE `barang_beli` DISABLE KEYS */;
INSERT INTO `barang_beli` (`id`, `tanggal`, `barang`, `jumlah`, `harga`, `status`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, '2017-06-07', 3, 1, 340000, 4, 1, '2017-06-07 07:01:22', 0, '0000-00-00 00:00:00'),
	(2, '2017-07-05', 3, 1, 340000, 1, 1, '2017-06-07 07:01:48', 0, '0000-00-00 00:00:00'),
	(3, '2017-06-07', 3, 1, 300000, 1, 1, '2017-06-07 07:02:30', 0, '0000-00-00 00:00:00'),
	(4, '2017-06-13', 4, 3, 120000, 1, 1, '2017-06-07 07:03:07', 0, '0000-00-00 00:00:00'),
	(5, '2017-08-15', 3, 2, 120000, 4, 1, '2017-06-07 07:03:17', 1, '2017-07-12 18:46:23'),
	(6, '2017-07-20', 5, 4, 1200000, 1, 1, '2017-06-07 07:03:29', 0, '0000-00-00 00:00:00'),
	(7, '2017-06-23', 3, 12, 340000, 1, 1, '2017-06-07 07:03:56', 0, '0000-00-00 00:00:00'),
	(8, '2017-05-10', 4, 3, 23000, 1, 1, '2017-06-07 07:04:07', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_beli` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_beli_status
CREATE TABLE IF NOT EXISTS `barang_beli_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_beli_status: ~4 rows (approximately)
/*!40000 ALTER TABLE `barang_beli_status` DISABLE KEYS */;
INSERT INTO `barang_beli_status` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Pending', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Proses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(3, 'Reject', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(4, 'Approve', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_beli_status` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_jenis
CREATE TABLE IF NOT EXISTS `barang_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_jenis: ~5 rows (approximately)
/*!40000 ALTER TABLE `barang_jenis` DISABLE KEYS */;
INSERT INTO `barang_jenis` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Komputer', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Elektronik', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(3, 'Office & Equipment', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(4, 'Furniture', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(5, 'Kendaraan', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_jenis` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_minta
CREATE TABLE IF NOT EXISTS `barang_minta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NOT NULL,
  `barang` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_minta: ~2 rows (approximately)
/*!40000 ALTER TABLE `barang_minta` DISABLE KEYS */;
INSERT INTO `barang_minta` (`id`, `tanggal`, `barang`, `jumlah`, `harga`, `status`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, '2017-07-11', 3, 13, 450000, 1, 1, '2017-07-12 18:53:04', 1, '2017-07-12 18:54:41'),
	(2, '2017-07-12', 3, 12, 222, 1, 1, '2017-07-12 19:02:57', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_minta` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_minta_status
CREATE TABLE IF NOT EXISTS `barang_minta_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_minta_status: ~4 rows (approximately)
/*!40000 ALTER TABLE `barang_minta_status` DISABLE KEYS */;
INSERT INTO `barang_minta_status` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Pending', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Proses', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(3, 'Reject', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(4, 'Approve', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_minta_status` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_pakai
CREATE TABLE IF NOT EXISTS `barang_pakai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `tanggal_pakai` date NOT NULL,
  `tanggal_rusak` date NOT NULL,
  `tanggal_buang` date NOT NULL,
  `tanggal_jual` date NOT NULL,
  `barang` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `kondisi` int(11) NOT NULL,
  `lokasi` int(11) NOT NULL,
  `pengguna` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_pakai: ~10 rows (approximately)
/*!40000 ALTER TABLE `barang_pakai` DISABLE KEYS */;
INSERT INTO `barang_pakai` (`id`, `label`, `tanggal_beli`, `tanggal_pakai`, `tanggal_rusak`, `tanggal_buang`, `tanggal_jual`, `barang`, `harga`, `kondisi`, `lokasi`, `pengguna`, `status`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, '1/A.1/06/2017/MP8', '2017-06-01', '2017-06-06', '0000-00-00', '0000-00-00', '0000-00-00', 3, 340000, 2, 1, 'adam', 1, 1, '2017-06-06 21:28:01', 1, '2017-06-07 06:26:43'),
	(2, '2/A.2/06/2017/MP5', '2017-06-06', '2017-06-06', '0000-00-00', '0000-00-00', '0000-00-00', 4, 450000, 1, 2, 'sandroy', 1, 1, '2017-06-06 21:30:13', 1, '2017-06-06 21:32:55'),
	(3, '3/B.1/06/2017/MP8', '2017-06-07', '2017-06-15', '0000-00-00', '0000-00-00', '0000-00-00', 5, 250000000, 1, 1, 'All', 1, 1, '2017-06-07 05:22:32', 0, '0000-00-00 00:00:00'),
	(4, '4/A.1/06/2017/MP8', '2017-06-07', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', 3, 350000, 1, 1, 'kiki', 1, 1, '2017-06-07 05:22:59', 0, '0000-00-00 00:00:00'),
	(5, '8/A.1/06/2017/MP5', '2017-06-07', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', 3, 340000, 1, 2, 'teguh', 1, 1, '2017-06-07 07:01:22', 1, '2017-07-12 21:18:48'),
	(6, '1/A.2/06/2017/MP8', '2017-06-07', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', 4, 100000, 1, 1, 'adam', 1, 1, '2017-06-07 19:47:47', 0, '0000-00-00 00:00:00'),
	(7, '4/A.2/06/2017/MP8', '2017-06-07', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', 4, 343434, 1, 1, 'adam', 1, 1, '2017-06-07 19:48:50', 1, '2017-07-12 21:18:59'),
	(8, '10/A.1/06/2017/MP8', '2017-06-07', '2017-06-07', '2017-07-12', '2017-07-13', '2017-07-14', 3, 230000, 2, 1, 'adam', 2, 1, '2017-06-07 19:49:12', 1, '2017-07-12 22:02:38'),
	(9, '9/A.1//MP8', '2017-08-15', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 3, 120000, 1, 1, '', 2, 1, '2017-07-12 18:46:23', 1, '2017-07-12 21:45:13'),
	(10, '', '2017-08-15', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', 3, 120000, 1, 0, '', 2, 1, '2017-07-12 18:46:23', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_pakai` ENABLE KEYS */;


-- Dumping structure for table inventaris.barang_pakai_status
CREATE TABLE IF NOT EXISTS `barang_pakai_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.barang_pakai_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `barang_pakai_status` DISABLE KEYS */;
INSERT INTO `barang_pakai_status` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Digunakan', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Tidak Digunakan', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(3, 'Dibuang', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(4, 'Dijual', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `barang_pakai_status` ENABLE KEYS */;


-- Dumping structure for table inventaris.kondisi
CREATE TABLE IF NOT EXISTS `kondisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.kondisi: ~4 rows (approximately)
/*!40000 ALTER TABLE `kondisi` DISABLE KEYS */;
INSERT INTO `kondisi` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Baik', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Rusak', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `kondisi` ENABLE KEYS */;


-- Dumping structure for table inventaris.lokasi
CREATE TABLE IF NOT EXISTS `lokasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.lokasi: ~2 rows (approximately)
/*!40000 ALTER TABLE `lokasi` DISABLE KEYS */;
INSERT INTO `lokasi` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Mampang 8', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Mampang 5', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `lokasi` ENABLE KEYS */;


-- Dumping structure for table inventaris.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.module: ~15 rows (approximately)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `name`, `url`, `icon`, `parent`, `order`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Beranda', 'home', 'fa fa-home', 0, 1, 1, '2016-12-15 22:24:48', 1, '2016-12-15 23:23:09'),
	(2, 'Data Master', '', 'fa fa-database', 0, 2, 1, '2016-12-15 22:25:55', 1, '2016-12-15 23:23:29'),
	(23, 'Level', 'users_level', '', 61, 2, 1, '2016-12-15 23:12:35', 1, '2017-04-29 11:15:58'),
	(24, 'Status', 'reference/users_status', '', 61, 3, 1, '2016-12-15 23:13:12', 1, '2017-04-29 11:15:48'),
	(25, 'Pengguna', 'users', '', 61, 1, 1, '2016-12-15 23:15:54', 1, '2017-04-29 11:15:38'),
	(26, 'Backup', 'backup', 'fa fa-download', 0, 8, 1, '2016-12-15 23:16:42', 1, '2016-12-16 00:21:36'),
	(27, 'Module', 'module', 'fa fa-check', 0, 7, 1, '2016-12-16 00:21:22', 0, '0000-00-00 00:00:00'),
	(61, 'Keamanan', '', 'fa fa-lock', 0, 4, 1, '2017-04-29 11:15:22', 1, '2017-04-29 11:53:10'),
	(62, 'Jenis Barang', 'reference/barang_jenis', '', 2, 2, 1, '2017-04-29 11:26:15', 1, '2017-04-29 11:44:08'),
	(63, 'Barang', 'barang', '', 2, 1, 1, '2017-04-29 11:44:28', 0, '0000-00-00 00:00:00'),
	(64, 'Transaksi', '', 'fa fa-tasks', 0, 3, 1, '2017-04-29 11:52:57', 0, '0000-00-00 00:00:00'),
	(65, 'Permintaan Barang', 'barang_minta', '', 64, 1, 1, '2017-04-29 11:54:27', 0, '0000-00-00 00:00:00'),
	(66, 'Pembelian Barang', 'barang_beli', '', 64, 2, 1, '2017-04-29 11:54:53', 0, '0000-00-00 00:00:00'),
	(67, 'Pemakaian Barang', 'barang_pakai', '', 64, 3, 1, '2017-04-29 11:55:26', 0, '0000-00-00 00:00:00'),
	(68, 'Laporan', 'report', 'fa fa-bar-chart', 0, 4, 1, '2017-04-29 13:40:04', 1, '2017-04-29 13:54:06');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;


-- Dumping structure for table inventaris.report
CREATE TABLE IF NOT EXISTS `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.report: ~5 rows (approximately)
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` (`id`, `jenis`, `date_from`, `date_to`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 1, '2017-05-01', '2017-05-31', 1, '2017-04-29 14:06:02', 1, '2017-05-18 08:49:05'),
	(2, 2, '2017-06-01', '2017-06-03', 1, '2017-06-03 11:11:34', 0, '0000-00-00 00:00:00'),
	(3, 3, '2017-06-01', '2017-06-30', 1, '2017-06-05 20:17:51', 0, '0000-00-00 00:00:00'),
	(4, 4, '2017-06-01', '2017-08-16', 1, '2017-06-07 05:20:16', 1, '2017-07-12 21:22:33'),
	(5, 5, '2017-06-01', '2017-08-31', 1, '2017-06-07 19:54:16', 1, '2017-07-12 21:47:16');
/*!40000 ALTER TABLE `report` ENABLE KEYS */;


-- Dumping structure for table inventaris.report_jenis
CREATE TABLE IF NOT EXISTS `report_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.report_jenis: ~5 rows (approximately)
/*!40000 ALTER TABLE `report_jenis` DISABLE KEYS */;
INSERT INTO `report_jenis` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Laporan Permintaan Barang', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(2, 'Laporan Pembelian Barang', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(3, 'Laporan Pemakaian Barang', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(4, 'Laporan Stok Barang', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
	(5, 'Laporan Barang Rusak', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `report_jenis` ENABLE KEYS */;


-- Dumping structure for table inventaris.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  `ip_login` varchar(50) NOT NULL,
  `date_login` datetime NOT NULL,
  `user_agent` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.users: ~1 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `username`, `password`, `level`, `ip_login`, `date_login`, `user_agent`, `status`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'Adam Prasetia', 'damz', '202cb962ac59075b964b07152d234b70', 1, '::1', '2017-07-12 18:42:10', 'Windows 7(Google Chrome 59.0.3071.115)', 1, 0, '0000-00-00 00:00:00', 2, '2016-10-21 09:34:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table inventaris.users_level
CREATE TABLE IF NOT EXISTS `users_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `module` text NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.users_level: ~1 rows (approximately)
/*!40000 ALTER TABLE `users_level` DISABLE KEYS */;
INSERT INTO `users_level` (`id`, `name`, `module`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'ADMIN', '1,2,63,62,64,65,66,67,61,25,23,24,68,27,26', 0, '0000-00-00 00:00:00', 1, '2017-04-29 14:05:17');
/*!40000 ALTER TABLE `users_level` ENABLE KEYS */;


-- Dumping structure for table inventaris.users_status
CREATE TABLE IF NOT EXISTS `users_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_create` int(11) NOT NULL,
  `date_create` datetime NOT NULL,
  `user_update` int(11) NOT NULL,
  `date_update` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table inventaris.users_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `users_status` DISABLE KEYS */;
INSERT INTO `users_status` (`id`, `name`, `user_create`, `date_create`, `user_update`, `date_update`) VALUES
	(1, 'ACTIVE', 0, '2015-10-31 14:00:03', 1, '2016-06-24 10:43:51'),
	(2, 'NOT ACTIVE', 0, '2015-10-31 14:00:03', 1, '2016-06-24 10:43:55');
/*!40000 ALTER TABLE `users_status` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
