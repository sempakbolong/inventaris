<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	private $data = array();
	function __construct()
	{
		parent::__construct();
		$this->data['title'] = config_item('app_name');
		$this->data['subtitle'] = 'Beranda';
		$this->data['module'] = 'home';		
	}
	public function index()
	{
		$this->load->model('barang_pakai/barang_pakai_model');
		$this->data['total_barang_mampang_8'] = $this->barang_pakai_model->total('lokasi',1);
		$this->data['total_barang_mampang_5'] = $this->barang_pakai_model->total('lokasi',2);
		$this->data['total_barang_baik'] = $this->barang_pakai_model->total('kondisi',1);
		$this->data['total_barang_rusak'] = $this->barang_pakai_model->total('kondisi',2);
		$this->data['content'] = $this->load->view('home_view',$this->data,true);
		$this->load->view('template_view',$this->data);
	}
	public function logout()
	{
		$this->session->unset_userdata('user_login');
		redirect('login');
	}
}