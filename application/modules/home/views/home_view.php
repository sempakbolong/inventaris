<link rel="stylesheet" href="<?php echo base_url("assets/plugins/AdminLTE-2.3.3/plugins/morris/morris.css") ?>">
<script type="text/javascript" src="<?php echo base_url("assets/plugins/AdminLTE-2.3.3/plugins/morris/morris.min.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/plugins/AdminLTE-2.3.3/plugins/morris/raphael-min.js") ?>"></script>

<div class="row">
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo $total_barang_mampang_8; ?></h3>

        <p>Barang Di Mampang 8</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo site_url('barang_pakai?lokasi=1') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $total_barang_mampang_5; ?></h3>

        <p>Barang Di Mampang 5</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo site_url('barang_pakai?lokasi=2') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $total_barang_baik; ?></h3>

        <p>Barang Kondisi Baik</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo site_url('barang_pakai?kondisi=1') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3><?php echo $total_barang_rusak; ?></h3>

        <p>Barang Kondisi Rusak</p>
      </div>
      <div class="icon">
        <i class="ion ion-bag"></i>
      </div>
      <a href="<?php echo site_url('barang_pakai?kondisi=2') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
</div>
<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Pembelian Barang</h3>
  </div>
  <div class="box-body chart-responsive">
    <div class="chart" id="line-chart" style="height: 300px;"></div>
  </div>
</div>
<script>
  $('document').ready(function(){
    $.ajax({
      url:'<?php echo site_url("barang_beli/line_chart") ?>',
      type:'post',
      dataType:'json',
      success:function(str){
          var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: str,
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Total'],
            lineColors: ['#3c8dbc'],
            hideHover: 'auto'
          });
      }
    });
  });
</script>