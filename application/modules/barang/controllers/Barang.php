<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends MY_Controller 
{
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->data['title'] = 'Barang';
		$this->data['subtitle'] = '';
		$this->data['module'] = 'barang';
		$this->load->model($this->data['module'].'/model','model');
	}
	public function index()
	{
		$offset = $this->general->get_offset();
		$limit = $this->general->get_limit();
		$total = $this->model->count_all();

		$this->table->set_template(tbl_tmp());
		$head_data = array(
			'code'=>'Kode',
			'name'=>'Nama',
			'jenis_name'=>'Jenis',
			'merk'=>'Merk',
			'stok'=>'Stok'
		);
		$heading[] = form_checkbox(array('id'=>'selectAll','value'=>1));
		$heading[] = '#';
		foreach($head_data as $r => $value){
			$heading[] = anchor($this->data['module'].get_query_string(array('order_column'=>"$r",'order_type'=>$this->general->order_type($r))),"$value ".$this->general->order_icon("$r"));
		}		
		$heading[] = $this->lang->line('action');
		$this->table->set_heading($heading);
		$result = $this->model->get()->result();
		$i=1+$offset;
		foreach($result as $r){
			$this->table->add_row(
				array('data'=>form_checkbox(array('name'=>'check[]','value'=>$r->id)),'width'=>'10px'),
				$i++,
				anchor($this->data['module'].'/edit/'.$r->id.get_query_string(),$r->code),
				$r->name,
				$r->jenis_name,
				$r->merk,
				number_format($r->stok),
				anchor($this->data['module'].'/edit/'.$r->id.get_query_string(),$this->lang->line('edit'),array('class'=>'btn btn-default btn-xs'))
				."&nbsp;|&nbsp;".anchor($this->data['module'].'/delete/'.$r->id.get_query_string(),$this->lang->line('delete'),array('class'=>'btn btn-danger btn-xs','onclick'=>"return confirm('".$this->lang->line('confirm')."')"))
			);
		}
		$this->data['table'] = $this->table->generate();
		$this->data['total'] = page_total($offset,$limit,$total);
		
		$config = pag_tmp();
		$config['base_url'] = site_url($this->data['module'].get_query_string(null,'offset'));
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$this->pagination->initialize($config); 
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['content'] = $this->load->view($this->data['module'].'/list',$this->data,true);
		$this->load->view('template_view',$this->data);
	}
	public function search()
	{
		$data = array(
			'search'=>$this->input->post('search'),
			'limit'=>$this->input->post('limit'),
			'jenis'=>$this->input->post('jenis')
		);
		redirect($this->data['module'].get_query_string($data));		
	}
	private function _field()
	{
		$data = array(
			'code'=>$this->input->post('code'),
			'name'=>$this->input->post('name'),
			'jenis'=>$this->input->post('jenis'),
			'merk'=>$this->input->post('merk')
		);
		return $data;		
	}
	private function _set_rules($id = '')
	{
		$this->form_validation->set_rules('code','Kode','required|trim|callback_check_code['.$id.']');
		$this->form_validation->set_rules('name','Name','required|trim');
		$this->form_validation->set_rules('jenis','Jenis','required|trim');
		$this->form_validation->set_rules('merk','Merk','trim');
	}
	public function check_code($str,$id)
	{
		$result = $this->general_model->double_check($this->data['module'],'code',$str,$id);
		if ($result) {
			$this->form_validation->set_message('check_code','Code sudah terdaftar');
			return FALSE;
		}
		return TRUE;
	}
	public function add()
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['action'] = $this->data['module'].'/add'.get_query_string();
			$this->data['owner'] = '';
			$this->data['confirm'] = 'Tambah data barang ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->add($this->data['module'],$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('new_success').'</div>');
			redirect($this->data['module'].'/add'.get_query_string());
		}
	}
	public function edit($id)
	{
		$this->_set_rules($id);
		if($this->form_validation->run()===false){
			$this->data['row'] = $this->general_model->get_from_field($this->data['module'],'id',$id)->row();
			$this->data['action'] = $this->data['module'].'/edit/'.$id.get_query_string();
			$this->data['owner'] = '<div class="box-header owner">'.owner($this->data['row']).'</div>';
			$this->data['confirm'] = 'Edit data barang ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->edit($this->data['module'],$id,$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('edit_success').'</div>');
			redirect($this->data['module'].'/edit/'.$id.get_query_string());
		}
	}
	public function delete($id='')
	{
		if($id<>''){
			$this->general_model->delete($this->data['module'],$id);
		}
		$check = $this->input->post('check');
		if($check<>''){
			foreach($check as $c){
				$this->general_model->delete($this->data['module'],$c);
			}
		}
		$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('delete_success').'</div>');
		redirect($this->data['module'].get_query_string());
	}
}