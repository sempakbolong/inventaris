<?php echo $this->session->flashdata('alert')?>
<form action="<?php echo site_url($action) ?>" method="post">
<div class="box box-default">
	<?php echo $owner; ?>
	<div class="box-body">
		<div class="form-group form-inline">
			<?php echo form_label('Kode','code',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'code','class'=>'form-control input-sm','maxlength'=>'10','size'=>'20','autocomplete'=>'off','value'=>set_value('code',(isset($row->code)?$row->code:''))))?>
			<small><?php echo form_error('code')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Nama','name',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'name','class'=>'form-control input-sm','maxlength'=>'100','size'=>'50','autocomplete'=>'off','value'=>set_value('name',(isset($row->name)?$row->name:''))))?>
			<small><?php echo form_error('name')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Jenis','jenis',array('class'=>'control-label'))?>
			<?php echo form_dropdown('jenis',$this->general_model->dropdown('barang_jenis','Jenis Barang'),set_value('jenis',(isset($row->jenis)?$row->jenis:'')),'id="jenis" class="form-control input-sm select2"')?>
			<small><?php echo form_error('jenis')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Merk','merk',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'merk','class'=>'form-control input-sm','maxlength'=>'100','size'=>'100','autocomplete'=>'off','value'=>set_value('merk',(isset($row->merk)?$row->merk:''))))?>
			<small><?php echo form_error('merk')?></small>
		</div>
	</div>
</div>
<div class="box box-default">	
	<div class="box-body">
		<button class="btn btn-success btn-sm" type="submit" onclick="return confirm('<?php echo $confirm ?>')"><span class="glyphicon glyphicon-save"></span> <?php echo $this->lang->line('save') ?></button>
		<a class="btn btn-danger btn-sm" href="<?php echo site_url($module.get_query_string()) ?>"><span class="glyphicon glyphicon-repeat"></span> <?php echo $this->lang->line('back') ?></a>		
	</div>
</div>
</form>