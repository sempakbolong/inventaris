<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

	private $tbl_name = 'barang';
	private $tbl_key = 'id';
	
	function query()
	{
		$data[] = $this->db->select(array(
			'a.*',
			'b.name as jenis_name',
			'count(c.id) as stok'
		));
		$data[] = $this->db->from($this->tbl_name.' a');
		$data[] = $this->db->join($this->tbl_name.'_jenis b','a.jenis = b.id','left');
		$data[] = $this->db->join($this->tbl_name.'_pakai c','a.id = c.barang and c.status=2 and c.kondisi=1','left');
		$search = $this->input->get('search');
		if($search <> ''){
			$data[] = $this->db->where('(a.code like "%'.$search.'%" or b.name like "%'.$search.'%" or a.name like "%'.$search.'%" or a.merk like "%'.$search.'%")');
		}		
		$jenis = $this->input->get('jenis');
		if($jenis <> ''){
			$data[] = $this->db->where('a.jenis',$jenis);
		}		
		$data[] = $this->db->group_by('a.id');
		$data[] = $this->db->order_by($this->general->get_order_column('a.id'),$this->general->get_order_type('asc'));
		$data[] = $this->db->offset($this->general->get_offset());
		return $data;
	}
	function get()
	{
		$this->query();
		$this->db->limit($this->general->get_limit());
		return $this->db->get();
	}
	function count_all()
	{
		$this->query();
		return $this->db->get()->num_rows();
	}
}