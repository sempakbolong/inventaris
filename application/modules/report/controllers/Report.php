<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller 
{
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->data['title'] = 'Laporan';
		$this->data['subtitle'] = '';
		$this->data['module'] = 'report';
		$this->load->model($this->data['module'].'/model','model');
	}
	public function index()
	{
		$offset = $this->general->get_offset();
		$limit = $this->general->get_limit();
		$total = $this->model->count_all();

		$this->table->set_template(tbl_tmp());
		$head_data = array(
			'jenis'=>'Jenis',
			'date_from'=>'Periode'
		);
		$heading[] = form_checkbox(array('id'=>'selectAll','value'=>1));
		$heading[] = '#';
		foreach($head_data as $r => $value){
			$heading[] = anchor($this->data['module'].get_query_string(array('order_column'=>"$r",'order_type'=>$this->general->order_type($r))),"$value ".$this->general->order_icon("$r"));
		}		
		$heading[] = $this->lang->line('action');
		$this->table->set_heading($heading);
		$result = $this->model->get()->result();
		$i=1+$offset;
		foreach($result as $r){
			$this->table->add_row(
				array('data'=>form_checkbox(array('name'=>'check[]','value'=>$r->id)),'width'=>'10px'),
				$i++,
				$r->jenis_name,
				dateformatindo($r->date_from,2).' s/d '.dateformatindo($r->date_to,2),
				anchor($this->data['module'].'/edit/'.$r->id.get_query_string(),$this->lang->line('edit'),array('class'=>'btn btn-default btn-xs'))
				."&nbsp;|&nbsp;".anchor($this->data['module'].'/pdf/'.$r->id,'Pdf',array('class'=>'btn btn-default btn-xs','target'=>'_blank'))
				."&nbsp;|&nbsp;".anchor($this->data['module'].'/delete/'.$r->id.get_query_string(),$this->lang->line('delete'),array('class'=>'btn btn-danger btn-xs','onclick'=>"return confirm('".$this->lang->line('confirm')."')"))
			);
		}
		$this->data['table'] = $this->table->generate();
		$this->data['total'] = page_total($offset,$limit,$total);
		
		$config = pag_tmp();
		$config['base_url'] = site_url($this->data['module'].get_query_string(null,'offset'));
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$this->pagination->initialize($config); 
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['content'] = $this->load->view($this->data['module'].'/list',$this->data,true);
		$this->load->view('template_view',$this->data);
	}
	public function search()
	{
		$data = array(
			'search'=>$this->input->post('search'),
			'limit'=>$this->input->post('limit'),
			'jenis'=>$this->input->post('jenis'),
			'date_from'=>$this->input->post('date_from'),
			'date_to'=>$this->input->post('date_to')
		);
		redirect($this->data['module'].get_query_string($data));		
	}
	private function _field()
	{
		$data = array(
			'jenis'=>$this->input->post('jenis'),
			'date_from'=>format_ymd($this->input->post('date_from')),
			'date_to'=>format_ymd($this->input->post('date_to'))
		);
		return $data;		
	}
	private function _set_rules()
	{
		$this->form_validation->set_rules('jenis','Jenis','required|trim');
		$this->form_validation->set_rules('date_from','Tanggal Dari','required|trim');
		$this->form_validation->set_rules('date_to','Tanggal Sampai','required|trim');
	}
	public function add()
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['action'] = $this->data['module'].'/add'.get_query_string();
			$this->data['owner'] = '';
			$this->data['confirm'] = 'Tambah report ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->add($this->data['module'],$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('new_success').'</div>');
			redirect($this->data['module'].'/add'.get_query_string());
		}
	}
	public function edit($id)
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['row'] = $this->general_model->get_from_field($this->data['module'],'id',$id)->row();
			$this->data['action'] = $this->data['module'].'/edit/'.$id.get_query_string();
			$this->data['owner'] = '<div class="box-header owner">'.owner($this->data['row']).'</div>';
			$this->data['confirm'] = 'Edit report ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->edit($this->data['module'],$id,$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('edit_success').'</div>');
			redirect($this->data['module'].'/edit/'.$id.get_query_string());
		}
	}
	public function delete($id='')
	{
		if($id<>''){
			$this->general_model->delete($this->data['module'],$id);
		}
		$check = $this->input->post('check');
		if($check<>''){
			foreach($check as $c){
				$this->general_model->delete($this->data['module'],$c);
			}
		}
		$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('delete_success').'</div>');
		redirect($this->data['module'].get_query_string());
	}
	public function pdf($id = '')
	{
		require_once "assets/plugins/fpdf/fpdf.php";		
		$pdf = new FPDF();
		$pdf->SetAutoPageBreak(TRUE, 10);
		$pdf->AliasNbPages();

		$title = '';

		$report = $this->general_model->get_from_field('report','id',$id)->row();
		/* Laporan Permintaan Barang */
		if ($report->jenis == 1) {
			
			$periode = '';
			if ($report->date_from && $report->date_to) {
				$periode = strtoupper('PERIODE : '.dateformatindo($report->date_from,2).' S/D '.dateformatindo($report->date_to,2));
			}

			$title = 'LAPORAN PERMINTAAN BARANG';

			$pdf->AddPage('L','A4');
			$pdf->SetTitle($title);

			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(0,5,$title,0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,5,'PT DATA BINA SOLUSINDO',0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,5,$periode,0,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,10,'NO',1,0,'C');
			$pdf->Cell(35,10,'TANGGAL',1,0,'C');
			$pdf->Cell(70,10,'NAMA BARANG',1,0,'C');
			$pdf->Cell(40,10,'JUMLAH',1,0,'C');
			$pdf->Cell(40,10,'ESTIMASI HARGA',1,0,'C');
			$pdf->Cell(40,10,'TOTAL',1,0,'C');
			$pdf->Cell(0,10,'STATUS',1,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',8);
			$i = 1;
			$total = 0;
			$this->load->model('barang_minta/barang_minta_model');
			$result = $this->barang_minta_model->report($report->date_from,$report->date_to)->result();
			foreach ($result as $b) {
				$pdf->Cell(10,5,$i,1,0,'C');
				$pdf->Cell(35,5,dateformatindo($b->tanggal,2),1,0,'L');
				$pdf->Cell(70,5,$b->barang_name,1,0,'L');
				$pdf->Cell(40,5,number_format($b->jumlah),1,0,'R');
				$pdf->Cell(40,5,number_format($b->harga),1,0,'R');
				$pdf->Cell(40,5,number_format($b->total),1,0,'R');
				$pdf->Cell(0,5,$b->status_name,1,0,'L');
				$pdf->Ln(5);
				$total += $b->total;
				$i++;
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(195,5,'TOTAL',1,0,'C');
			$pdf->Cell(40,5,number_format($total),1,0,'R');
			$pdf->Cell(0,5,'',1,0,'R');
			$pdf->Output($title,"I");	
		}elseif ($report->jenis == 2) { // Laporan pembelian barang
			
			$periode = '';
			if ($report->date_from && $report->date_to) {
				$periode = strtoupper('PERIODE : '.dateformatindo($report->date_from,2).' S/D '.dateformatindo($report->date_to,2));
			}

			$title = 'LAPORAN PEMBELIAN BARANG';

			$pdf->AddPage('L','A4');
			$pdf->SetTitle($title);

			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(0,5,$title,0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,5,'PT DATA BINA SOLUSINDO',0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,5,$periode,0,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,10,'NO',1,0,'C');
			$pdf->Cell(35,10,'TANGGAL',1,0,'C');
			$pdf->Cell(70,10,'NAMA BARANG',1,0,'C');
			$pdf->Cell(40,10,'JUMLAH',1,0,'C');
			$pdf->Cell(40,10,'HARGA',1,0,'C');
			$pdf->Cell(40,10,'TOTAL',1,0,'C');
			$pdf->Cell(0,10,'STATUS',1,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',8);
			$i = 1;
			$total = 0;
			$this->load->model('barang_beli/barang_beli_model');
			$result = $this->barang_beli_model->report($report->date_from,$report->date_to)->result();
			foreach ($result as $b) {
				$pdf->Cell(10,5,$i,1,0,'C');
				$pdf->Cell(35,5,dateformatindo($b->tanggal,2),1,0,'L');
				$pdf->Cell(70,5,$b->barang_name,1,0,'L');
				$pdf->Cell(40,5,number_format($b->jumlah),1,0,'R');
				$pdf->Cell(40,5,number_format($b->harga),1,0,'R');
				$pdf->Cell(40,5,number_format($b->total),1,0,'R');
				$pdf->Cell(0,5,$b->status_name,1,0,'L');
				$pdf->Ln(5);
				$total += $b->total;
				$i++;
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(195,5,'TOTAL',1,0,'C');
			$pdf->Cell(40,5,number_format($total),1,0,'R');
			$pdf->Cell(0,5,'',1,0,'R');
			$pdf->Output($title,"I");	
		}elseif ($report->jenis == 3) { // Laporan pemakaian barang
			
			$periode = '';
			if ($report->date_from && $report->date_to) {
				$periode = strtoupper('PERIODE : '.dateformatindo($report->date_from,2).' S/D '.dateformatindo($report->date_to,2));
			}

			$title = 'LAPORAN PEMAKAIAN BARANG';

			$pdf->AddPage('L','A4');
			$pdf->SetTitle($title);

			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(0,5,$title,0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,5,'PT DATA BINA SOLUSINDO',0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,5,$periode,0,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,10,'NO',1,0,'C');
			$pdf->Cell(35,10,'TANGGAL',1,0,'C');
			$pdf->Cell(40,10,'LABEL',1,0,'C');
			$pdf->Cell(45,10,'NAMA BARANG',1,0,'C');
			$pdf->Cell(35,10,'HARGA',1,0,'C');
			$pdf->Cell(35,10,'KONDISI',1,0,'C');
			$pdf->Cell(35,10,'PENGGUNA',1,0,'C');
			$pdf->Cell(0,10,'STATUS',1,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',8);
			$i = 1;
			$total = 0;
			$this->load->model('barang_pakai/barang_pakai_model');
			$result = $this->barang_pakai_model->report($report->date_from,$report->date_to)->result();
			foreach ($result as $b) {
				$pdf->Cell(10,5,$i,1,0,'C');
				$pdf->Cell(35,5,dateformatindo($b->tanggal_pakai,2),1,0,'L');
				$pdf->Cell(40,5,$b->label,1,0,'L');
				$pdf->Cell(45,5,$b->barang_name,1,0,'L');
				$pdf->Cell(35,5,number_format($b->harga),1,0,'R');
				$pdf->Cell(35,5,$b->kondisi_name,1,0,'L');
				$pdf->Cell(35,5,$b->pengguna,1,0,'L');
				$pdf->Cell(0,5,$b->status_name,1,0,'L');
				$pdf->Ln(5);
				$total += $b->harga;
				$i++;
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(130,5,'TOTAL',1,0,'C');
			$pdf->Cell(35,5,number_format($total),1,0,'R');
			$pdf->Cell(0,5,'',1,0,'R');
			$pdf->Output($title,"I");	
		}elseif ($report->jenis == 4) { // Laporan stok barang
			
			$periode = '';
			if ($report->date_from && $report->date_to) {
				$periode = strtoupper('TANGGAL : '.dateformatindo(date('Y-m-d'),2));
			}

			$title = 'LAPORAN STOK BARANG';

			$pdf->AddPage('P','A4');
			$pdf->SetTitle($title);

			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(0,5,$title,0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,5,'PT DATA BINA SOLUSINDO',0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,5,$periode,0,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,10,'NO',1,0,'C');
			$pdf->Cell(35,10,'KODE BARANG',1,0,'C');
			$pdf->Cell(70,10,'NAMA BARANG',1,0,'C');
			$pdf->Cell(40,10,'JENIS BARANG',1,0,'C');
			$pdf->Cell(0,10,'STOK',1,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',8);
			$i = 1;
			$total = 0;
			$this->load->model('barang_pakai/barang_pakai_model');
			$result = $this->barang_pakai_model->report_stok()->result();
			foreach ($result as $b) {
				$pdf->Cell(10,5,$i,1,0,'C');
				$pdf->Cell(35,5,$b->barang_code,1,0,'L');
				$pdf->Cell(70,5,$b->barang_name,1,0,'L');
				$pdf->Cell(40,5,$b->jenis_name,1,0,'L');
				$pdf->Cell(0,5,number_format($b->stok),1,0,'R');
				$pdf->Ln(5);
				$total += $b->stok;
				$i++;
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(155,5,'TOTAL',1,0,'C');
			$pdf->Cell(0,5,number_format($total),1,0,'R');
			$pdf->Output($title,"I");	
		}elseif ($report->jenis == 5) { // Laporan barang rusak
			
			$periode = '';
			if ($report->date_from && $report->date_to) {
				$periode = strtoupper('PERIODE : '.dateformatindo($report->date_from,2).' S/D '.dateformatindo($report->date_to,2));
			}

			$title = 'LAPORAN BARANG RUSAK';

			$pdf->AddPage('L','A4');
			$pdf->SetTitle($title);

			$pdf->SetFont('Arial','B',14);
			$pdf->Cell(0,5,$title,0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',12);
			$pdf->Cell(0,5,'PT DATA BINA SOLUSINDO',0,0,'C');
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',10);
			$pdf->Cell(0,5,$periode,0,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(10,10,'NO',1,0,'C');
			$pdf->Cell(35,10,'TANGGAL',1,0,'C');
			$pdf->Cell(40,10,'LABEL',1,0,'C');
			$pdf->Cell(45,10,'NAMA BARANG',1,0,'C');
			$pdf->Cell(35,10,'HARGA',1,0,'C');
			$pdf->Cell(35,10,'KONDISI',1,0,'C');
			$pdf->Cell(35,10,'PENGGUNA',1,0,'C');
			$pdf->Cell(0,10,'STATUS',1,0,'C');
			$pdf->Ln(10);
			$pdf->SetFont('Arial','',8);
			$i = 1;
			$total = 0;
			$this->load->model('barang_pakai/barang_pakai_model');
			$result = $this->barang_pakai_model->report_rusak($report->date_from,$report->date_to)->result();
			foreach ($result as $b) {
				$pdf->Cell(10,5,$i,1,0,'C');
				$pdf->Cell(35,5,dateformatindo($b->tanggal_rusak,2),1,0,'L');
				$pdf->Cell(40,5,$b->label,1,0,'L');
				$pdf->Cell(45,5,$b->barang_name,1,0,'L');
				$pdf->Cell(35,5,number_format($b->harga),1,0,'R');
				$pdf->Cell(35,5,$b->kondisi_name,1,0,'L');
				$pdf->Cell(35,5,$b->pengguna,1,0,'L');
				$pdf->Cell(0,5,$b->status_name,1,0,'L');
				$pdf->Ln(5);
				$total += $b->harga;
				$i++;
			}
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(130,5,'TOTAL',1,0,'C');
			$pdf->Cell(35,5,number_format($total),1,0,'R');
			$pdf->Cell(0,5,'',1,0,'R');
			$pdf->Output($title,"I");	
		}	
	}	
}