<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model extends CI_Model {

	private $tbl_name = 'report';
	private $tbl_key = 'id';
	
	function query()
	{
		$data[] = $this->db->select(array(
			'a.*',
			'b.name as jenis_name'
		));
		$data[] = $this->db->from($this->tbl_name.' a');
		$data[] = $this->db->join($this->tbl_name.'_jenis b','a.jenis = b.id','left');
		$search = $this->input->get('search');
		if($search <> ''){
			$data[] = $this->db->where('(b.name like "%'.$search.'%")');
		}		
		$jenis = $this->input->get('jenis');
		if($jenis <> ''){
			$data[] = $this->db->where('a.jenis',$jenis);
		}		
		if($this->input->get('date_from') <> '' && $this->input->get('date_to') <> ''){			
			$data[] = $this->db->where('a.date_from >=',format_ymd($this->input->get('date_from')));
			$data[] = $this->db->where('a.date_to <=',format_ymd($this->input->get('date_to')));
		}		
		$data[] = $this->db->order_by($this->general->get_order_column('a.id'),$this->general->get_order_type('desc'));
		$data[] = $this->db->offset($this->general->get_offset());
		return $data;
	}
	function get()
	{
		$this->query();
		$this->db->limit($this->general->get_limit());
		return $this->db->get();
	}
	function count_all()
	{
		$this->query();
		return $this->db->get()->num_rows();
	}
}