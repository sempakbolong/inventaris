<?php echo $this->session->flashdata('alert')?>
<form action="<?php echo site_url($action) ?>" method="post">
<div class="box box-default">
	<?php echo $owner; ?>
	<div class="box-body">
		<div class="form-group form-inline">
			<?php echo form_label('Jenis','jenis',array('class'=>'control-label'))?>
			<?php echo form_dropdown('jenis',$this->general_model->dropdown('report_jenis','Jenis'),set_value('jenis',(isset($row->jenis)?$row->jenis:'')),'id="jenis" class="form-control input-sm select2"')?>
			<small><?php echo form_error('jenis')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Periode Tanggal','date_from',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'date_from','class'=>'form-control input-sm input-tanggal','maxlength'=>'10','size'=>'10','autocomplete'=>'off','value'=>set_value('date_from',(isset($row->date_from)?format_dmy($row->date_from):date('d/m/Y')))))?>
			<?php echo form_input(array('name'=>'date_to','class'=>'form-control input-sm input-tanggal','maxlength'=>'10','size'=>'10','autocomplete'=>'off','value'=>set_value('date_to',(isset($row->date_to)?format_dmy($row->date_to):date('d/m/Y')))))?>
			<small><?php echo form_error('date_from')?></small>
			<small><?php echo form_error('date_to')?></small>
		</div>
	</div>
</div>
<div class="box box-default">	
	<div class="box-body">
		<button class="btn btn-success btn-sm" type="submit" onclick="return confirm('<?php echo $confirm ?>')"><span class="glyphicon glyphicon-save"></span> <?php echo $this->lang->line('save') ?></button>
		<a class="btn btn-danger btn-sm" href="<?php echo site_url($module.get_query_string()) ?>"><span class="glyphicon glyphicon-repeat"></span> <?php echo $this->lang->line('back') ?></a>		
	</div>
</div>
</form>