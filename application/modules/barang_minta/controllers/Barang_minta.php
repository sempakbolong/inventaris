<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_minta extends MY_Controller 
{
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->data['title'] = 'Permintaan Barang';
		$this->data['subtitle'] = '';
		$this->data['module'] = 'barang_minta';
		$this->load->model($this->data['module'].'/'.$this->data['module'].'_model','model');
		$user_login = $this->session->userdata('user_login');
		$this->data['level'] = $user_login['level'];
	}
	public function index()
	{
		$offset = $this->general->get_offset();
		$limit = $this->general->get_limit();
		$total = $this->model->count_all();

		$this->table->set_template(tbl_tmp());
		$head_data = array(
			'id'=>'Nomor',
			'tanggal'=>'Tanggal',
			'barang_name'=>'Nama Barang',
			'jumlah'=>'Jumlah',
			'harga'=>'Estimasi Harga',
			'total'=>'Total',
			'status_name'=>'Status'
		);
		$heading[] = form_checkbox(array('id'=>'selectAll','value'=>1));
		$heading[] = '#';
		foreach($head_data as $r => $value){
			$heading[] = anchor($this->data['module'].get_query_string(array('order_column'=>"$r",'order_type'=>$this->general->order_type($r))),"$value ".$this->general->order_icon("$r"));
		}		
		$heading[] = $this->lang->line('action');
		$this->table->set_heading($heading);
		$result = $this->model->get()->result();
		$i=1+$offset;
		foreach($result as $r){
			$this->table->add_row(
				array('data'=>form_checkbox(array('name'=>'check[]','value'=>$r->id)),'width'=>'10px'),
				$i++,
				$r->id,
				dateformatindo($r->tanggal,2),				
				$r->barang_name,
				number_format($r->jumlah),
				number_format($r->harga),
				number_format($r->total),
				$r->status_name,
				anchor($this->data['module'].'/edit/'.$r->id.get_query_string(),$this->lang->line('edit'),array('class'=>'btn btn-default btn-xs'))
				."&nbsp;|&nbsp;".anchor($this->data['module'].'/delete/'.$r->id.get_query_string(),$this->lang->line('delete'),array('class'=>'btn btn-danger btn-xs','onclick'=>"return confirm('".$this->lang->line('confirm')."')"))
			);
		}
		$this->data['table'] = $this->table->generate();
		$this->data['total'] = page_total($offset,$limit,$total);
		
		$config = pag_tmp();
		$config['base_url'] = site_url($this->data['module'].get_query_string(null,'offset'));
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$this->pagination->initialize($config); 
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['content'] = $this->load->view($this->data['module'].'/list',$this->data,true);
		$this->load->view('template_view',$this->data);
	}
	public function search()
	{
		$data = array(
			'limit'=>$this->input->post('limit'),
			'id'=>$this->input->post('id'),
			'barang'=>$this->input->post('barang'),
			'date_from'=>$this->input->post('date_from'),
			'date_to'=>$this->input->post('date_to'),
			'jenis'=>$this->input->post('jenis'),
			'status'=>$this->input->post('status')
		);
		redirect($this->data['module'].get_query_string($data));		
	}
	private function _field()
	{
		$data = array(
			'tanggal'=>format_ymd($this->input->post('tanggal')),
			'barang'=>$this->input->post('barang'),
			'jumlah'=>format_uang($this->input->post('jumlah')),
			'harga'=>format_uang($this->input->post('harga')),
			'status'=>$this->input->post('status')
		);
		return $data;		
	}
	private function _set_rules()
	{
		$this->form_validation->set_rules('tanggal','Tanggal','required|trim');
		$this->form_validation->set_rules('barang','Nama Barang','required|trim');
		$this->form_validation->set_rules('jumlah','Jumlah','required|trim');
		$this->form_validation->set_rules('harga','Harga','required|trim');
		$this->form_validation->set_rules('status','Status','required|trim');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
	}
	public function add()
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['action'] = $this->data['module'].'/add'.get_query_string();
			$this->data['owner'] = '';
			$this->data['confirm'] = 'Tambah data permintaan ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->add($this->data['module'],$data);
			if ($data['status'] == 4) {
				$beli = array(
					'tanggal'=>date('Y-m-d H:i:s'),
					'barang'=>$data['barang'],
					'jumlah'=>$data['jumlah'],
					'harga'=>$data['harga'],
					'status'=>1
				);				
				$this->general_model->add('barang_beli',$beli);
			}			
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('new_success').'</div>');
			redirect($this->data['module'].'/add'.get_query_string());
		}
	}
	public function edit($id)
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['row'] = $this->general_model->get_from_field($this->data['module'],'id',$id)->row();
			$this->data['action'] = $this->data['module'].'/edit/'.$id.get_query_string();
			$this->data['owner'] = '<div class="box-header owner">'.owner($this->data['row']).'</div>';
			$this->data['confirm'] = 'Edit data permintaan ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$status_before = $this->general_model->get_from_field($this->data['module'],'id',$id)->row()->status;
			if ($data['status'] == 4 && $status_before <> 4) {
				$beli = array(
					'tanggal'=>date('Y-m-d H:i:s'),
					'barang'=>$data['barang'],
					'jumlah'=>$data['jumlah'],
					'harga'=>$data['harga'],
					'status'=>1
				);
				$this->general_model->add('barang_beli',$beli);				
			}			
			$this->general_model->edit($this->data['module'],$id,$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('edit_success').'</div>');
			redirect($this->data['module'].'/edit/'.$id.get_query_string());
		}
	}
	public function delete($id='')
	{
		if($id<>''){
			$this->general_model->delete($this->data['module'],$id);
		}
		$check = $this->input->post('check');
		if($check<>''){
			foreach($check as $c){
				$this->general_model->delete($this->data['module'],$c);
			}
		}
		$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('delete_success').'</div>');
		redirect($this->data['module'].get_query_string());
	}
}