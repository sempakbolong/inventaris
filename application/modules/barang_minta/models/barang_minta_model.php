<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_minta_model extends CI_Model {

	private $tbl_name = 'barang_minta';
	private $tbl_key = 'id';
	
	function query()
	{
		$data[] = $this->db->select(array(
			'a.*',
			'b.name as barang_name',
			'c.name as status_name',
			'(a.jumlah*a.harga) as total'
		));
		$data[] = $this->db->from($this->tbl_name.' a');
		$data[] = $this->db->join('barang b','a.barang = b.id','left');
		$data[] = $this->db->join($this->tbl_name.'_status c','a.status = c.id','left');
		$id = $this->input->get('id');
		if($id <> ''){
			$data[] = $this->db->where('a.id',$id);
		}		
		$barang = $this->input->get('barang');
		if($barang <> ''){
			$data[] = $this->db->where('a.barang',$barang);
		}		
		$status = $this->input->get('status');
		if($status <> ''){
			$data[] = $this->db->where('a.status',$status);
		}		
		if($this->input->get('date_from') <> '' && $this->input->get('date_to') <> ''){			
			$data[] = $this->db->where('a.tanggal >=',format_ymd($this->input->get('date_from')));
			$data[] = $this->db->where('a.tanggal <=',format_ymd($this->input->get('date_to')));
		}		
		$data[] = $this->db->order_by($this->general->get_order_column('a.id'),$this->general->get_order_type('desc'));
		$data[] = $this->db->offset($this->general->get_offset());
		return $data;
	}
	function get()
	{
		$this->query();
		$this->db->limit($this->general->get_limit());
		return $this->db->get();
	}
	function report($date_from = '',$date_to = '')
	{
		$this->query();
		if($date_from <> '' && $date_to <> ''){			
			$this->db->where('a.tanggal >=',$date_from);
			$this->db->where('a.tanggal <=',$date_to);
		}		
		return $this->db->get();
	}
	function count_all()
	{
		$this->query();
		return $this->db->get()->num_rows();
	}
}