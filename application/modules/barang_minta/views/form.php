<?php echo $this->session->flashdata('alert')?>
<form action="<?php echo site_url($action) ?>" method="post">
<div class="box box-default">
	<?php echo $owner; ?>
	<div class="box-body">
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal','tanggal',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal','class'=>'form-control input-sm','size'=>'10','maxlength'=>'10','autocomplete'=>'off','readonly'=>'true','value'=>set_value('tanggal',(isset($row->tanggal)?format_dmy($row->tanggal):date('d/m/Y')))))?>
			<small><?php echo form_error('tanggal')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Barang','barang',array('class'=>'control-label'))?>
			<?php echo form_dropdown('barang',$this->general_model->dropdown('barang','Nama Barang'),set_value('barang',(isset($row->barang)?$row->barang:'')),'id="barang" class="form-control input-sm select2"')?>
			<small><?php echo form_error('barang')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Jumlah','jumlah',array('class'=>'control-label'))?>
			<?php echo form_input(array('id'=>'jumlah','name'=>'jumlah','class'=>'form-control input-sm input-uang','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('jumlah',(isset($row->jumlah)?$row->jumlah:''))))?>
			<small><?php echo form_error('jumlah')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Estimasi Harga','harga',array('class'=>'control-label'))?>
			<?php echo form_input(array('id'=>'harga','name'=>'harga','class'=>'form-control input-sm input-uang','maxlength'=>'20','autocomplete'=>'off','value'=>set_value('harga',(isset($row->harga)?$row->harga:''))))?>
			<small><?php echo form_error('harga')?></small>
		</div>
		<div class="form-group form-inline">
			<label for="total_price" class="control-label">Total</label>
			<input id="total" type="text" name="total" class="form-control input-sm input-uang" readonly="true">
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Status','status',array('class'=>'control-label'))?>
			<?php 
				$status = $this->general_model->dropdown('barang_minta_status','Status');
				if ($level==2) {
					unset($status[3]);
					unset($status[4]);					
				}
				echo form_dropdown('status',$status,set_value('status',(isset($row->status)?$row->status:'1')),'id="status" class="form-control input-sm select2"');
			?>
			<small><?php echo form_error('status')?></small>
		</div>
	</div>
</div>
<div class="box box-default">	
	<div class="box-body">
		<button class="btn btn-success btn-sm" type="submit" onclick="return confirm('<?php echo $confirm ?>')"><span class="glyphicon glyphicon-save"></span> <?php echo $this->lang->line('save') ?></button>
		<a class="btn btn-danger btn-sm" href="<?php echo site_url($module.get_query_string()) ?>"><span class="glyphicon glyphicon-repeat"></span> <?php echo $this->lang->line('back') ?></a>		
	</div>
</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		total();
		$('#jumlah').change(function(){
			total();
		});
		$('#harga').change(function(){
			total();
		});
	});
	function number_format(user_input){
	    var filtered_number = user_input.replace(/[^0-9]/gi, '');
	    var length = filtered_number.length;
	    var breakpoint = 1;
	    var formated_number = '';

	    for(i = 1; i <= length; i++){
	        if(breakpoint > 3){
	            breakpoint = 1;
	            formated_number = ',' + formated_number;
	        }
	        var next_letter = i + 1;
	        formated_number = filtered_number.substring(length - i, length - (i - 1)) + formated_number; 

	        breakpoint++;
	    }

	    return formated_number;
	}
	function total(){
		var jumlah = $('#jumlah').val().replace(/,/g,'');
		var harga = $('#harga').val().replace(/,/g,'');
	    var total = jumlah*harga;

		$('#total').val(number_format(total.toString()));
	}
</script>