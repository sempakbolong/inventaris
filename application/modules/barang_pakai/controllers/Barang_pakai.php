<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_pakai extends MY_Controller 
{
	private $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->data['title'] = 'Pemakaian Barang';
		$this->data['subtitle'] = '';
		$this->data['module'] = 'barang_pakai';
		$this->load->model($this->data['module'].'/'.$this->data['module'].'_model','model');
	}
	public function index()
	{
		$offset = $this->general->get_offset();
		$limit = $this->general->get_limit();
		$total = $this->model->count_all();

		$this->table->set_template(tbl_tmp());
		$head_data = array(
			'label'=>'Label',
			'tanggal_beli'=>'Tanggal Beli',
			'tanggal_pakai'=>'Tanggal Pakai',
			'barang_name'=>'Nama Barang',
			'harga'=>'Harga',
			'kondisi_name'=>'Kondisi',
			'lokasi_name'=>'Lokasi',
			'pengguna'=>'Pengguna',
			'status_name'=>'Status'
		);
		$heading[] = form_checkbox(array('id'=>'selectAll','value'=>1));
		$heading[] = '#';
		foreach($head_data as $r => $value){
			$heading[] = anchor($this->data['module'].get_query_string(array('order_column'=>"$r",'order_type'=>$this->general->order_type($r))),"$value ".$this->general->order_icon("$r"));
		}
		$heading[] = $this->lang->line('action');
		$this->table->set_heading($heading);
		$result = $this->model->get()->result();
		$i=1+$offset;
		foreach($result as $r){
			$this->table->add_row(
				array('data'=>form_checkbox(array('name'=>'check[]','value'=>$r->id)),'width'=>'10px'),
				$i++,
				$r->label,
				dateformatindo($r->tanggal_beli,2),
				dateformatindo($r->tanggal_pakai,2),
				$r->barang_name,
				number_format($r->harga),
				$r->kondisi_name,
				$r->lokasi_name,
				$r->pengguna,
				$r->status_name,
				anchor($this->data['module'].'/edit/'.$r->id.get_query_string(),$this->lang->line('edit'),array('class'=>'btn btn-default btn-xs'))
				."&nbsp;|&nbsp;".anchor($this->data['module'].'/delete/'.$r->id.get_query_string(),$this->lang->line('delete'),array('class'=>'btn btn-danger btn-xs','onclick'=>"return confirm('".$this->lang->line('confirm')."')"))
			);
		}
		$this->data['table'] = $this->table->generate();
		$this->data['total'] = page_total($offset,$limit,$total);
		
		$config = pag_tmp();
		$config['base_url'] = site_url($this->data['module'].get_query_string(null,'offset'));
		$config['total_rows'] = $total;
		$config['per_page'] = $limit;

		$this->pagination->initialize($config); 
		$this->data['pagination'] = $this->pagination->create_links();

		$this->data['content'] = $this->load->view($this->data['module'].'/list',$this->data,true);
		$this->load->view('template_view',$this->data);
	}
	public function search()
	{
		$data = array(
			'label'=>$this->input->post('label'),
			'limit'=>$this->input->post('limit'),
			'barang'=>$this->input->post('barang'),
			'date_from'=>$this->input->post('date_from'),
			'date_to'=>$this->input->post('date_to'),
			'jenis'=>$this->input->post('jenis'),
			'status'=>$this->input->post('status')
		);
		redirect($this->data['module'].get_query_string($data));		
	}
	private function _field()
	{
		$data = array(			
			'tanggal_beli'=>format_ymd($this->input->post('tanggal_beli')),
			'tanggal_pakai'=>format_ymd($this->input->post('tanggal_pakai')),
			'tanggal_rusak'=>format_ymd($this->input->post('tanggal_rusak')),
			'tanggal_buang'=>format_ymd($this->input->post('tanggal_buang')),
			'tanggal_jual'=>format_ymd($this->input->post('tanggal_jual')),
			'barang'=>$this->input->post('barang'),
			'harga'=>format_uang($this->input->post('harga')),
			'kondisi'=>$this->input->post('kondisi'),
			'lokasi'=>$this->input->post('lokasi'),
			'pengguna'=>$this->input->post('pengguna'),
			'status'=>$this->input->post('status')
		);
		if ($this->input->post('label')) {
			$data['label'] = $this->input->post('label');
		}else{			
			$data['label'] = $this->label($data['barang']).'/'.$this->barang($data['barang']).'/'.dateformatindo($data['tanggal_pakai'],6).'/'.$this->location($data['lokasi']);
		}
		return $data;		
	}
	private function barang($barang)
	{
		return $this->general_model->get_from_field('barang','id',$barang)->row()->code;
	}
	private function label($barang)
	{
		return $this->model->label($barang);
	}
	private function location($lokasi)
	{
		if ($lokasi == 1) {
			return 'MP8';
		}else if($lokasi == 2){
			return 'MP5';
		}else{
			return '';
		}
	}
	private function _set_rules()
	{
		$this->form_validation->set_rules('label','Label','trim');
		$this->form_validation->set_rules('tanggal_beli','Tanggal Beli','required|trim');
		$this->form_validation->set_rules('tanggal_pakai','Tanggal Pakai','trim');
		$this->form_validation->set_rules('tanggal_rusak','Tanggal Rusak','trim');
		$this->form_validation->set_rules('tanggal_buang','Tanggal Buang','trim');
		$this->form_validation->set_rules('tanggal_jual','Tanggal Jual','trim');
		$this->form_validation->set_rules('barang','Nama Barang','required|trim');
		$this->form_validation->set_rules('harga','Harga','required|trim');
		$this->form_validation->set_rules('kondisi','Kondisi','required|trim');
		$this->form_validation->set_rules('lokasi','Lokasi','required|trim');
		$this->form_validation->set_rules('pengguna','Pengguna','trim');
		$this->form_validation->set_rules('status','Status','required|trim');
		$this->form_validation->set_error_delimiters('<p class="error">','</p>');
	}
	public function add()
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['action'] = $this->data['module'].'/add'.get_query_string();
			$this->data['owner'] = '';
			$this->data['confirm'] = 'Tambah data pemakaian ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->add($this->data['module'],$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('new_success').'</div>');
			redirect($this->data['module'].'/add'.get_query_string());
		}
	}
	public function edit($id)
	{
		$this->_set_rules();
		if($this->form_validation->run()===false){
			$this->data['row'] = $this->general_model->get_from_field($this->data['module'],'id',$id)->row();
			$this->data['action'] = $this->data['module'].'/edit/'.$id.get_query_string();
			$this->data['owner'] = '<div class="box-header owner">'.owner($this->data['row']).'</div>';
			$this->data['confirm'] = 'Edit data pemakaian ?';
			$this->data['content'] = $this->load->view($this->data['module'].'/form',$this->data,true);
			$this->load->view('template_view',$this->data);
		}else{
			$data = $this->_field();
			$this->general_model->edit($this->data['module'],$id,$data);
			$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('edit_success').'</div>');
			redirect($this->data['module'].'/edit/'.$id.get_query_string());
		}
	}
	public function delete($id='')
	{
		if($id<>''){
			$this->general_model->delete($this->data['module'],$id);
		}
		$check = $this->input->post('check');
		if($check<>''){
			foreach($check as $c){
				$this->general_model->delete($this->data['module'],$c);
			}
		}
		$this->session->set_flashdata('alert','<div class="alert alert-success">'.$this->lang->line('delete_success').'</div>');
		redirect($this->data['module'].get_query_string());
	}
	public function pdf_label()
	{
		require_once "assets/plugins/fpdf/fpdf.php";		
		$pdf = new FPDF();				
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(80);
		$pdf->AddPage('P','A4');
		$pdf->SetFont('Arial','B',8);

		$result = $this->model->get_all()->result();

		$baris = 0;
		$ln = 0;
		$x = 1;
		$y = 1;
		foreach($result as $r){
			$pdf->setxy($x,$y);
			$pdf->SetLineWidth(0.5);
			$pdf->rect($x,$y,40,10);
			$pdf->SetLineWidth(0);
			$pdf->image('assets/img/adirect_label.png',$x,$y,40,10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(40,5,"PT. DATA BINA SOLUSINDO",1,2,'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(40,5,$r->label,1,1,'C');
			$x = $x + 42;
			$ln++;
			if($ln==5){
				$x = 1;
				$y = $y+12;
				$ln = 0;
				$baris++;
			}
			if($baris==24){
				$baris = 0;
				$x = 1;
				$y = 1;
				$pdf->AddPage('P','A4');
			}
		}

		$pdf->Output();
	}		
	public function pdf_empty_label()
	{
		require_once "assets/plugins/fpdf/fpdf.php";		
		$pdf = new FPDF();		
		$pdf->AliasNbPages();
		$pdf->SetAutoPageBreak(80);
		$pdf->AddPage('P','A4');
		$pdf->SetFont('Arial','B',8);

		$baris = 0;
		$ln = 0;
		$x = 1;
		$y = 1;
		for($i=1;$i<=115;$i++){
			$pdf->setxy($x,$y);
			$pdf->SetLineWidth(0.5);
			$pdf->rect($x,$y,40,10);
			$pdf->SetLineWidth(0);
			$pdf->image('assets/img/adirect_label.png',$x,$y,40,10);
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(40,5,"PT. DATA BINA SOLUSINDO",1,2,'C');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(40,5,'',1,1,'C');
			$x = $x + 42;
			$ln++;
			if($ln==5){
				$x = 1;
				$y = $y+12;
				$ln = 0;
				$baris++;
			}
			if($baris==24){
				$baris = 0;
				$x = 1;
				$y = 1;
				$pdf->AddPage('P','A4');
			}
		}

		$pdf->Output();
	}	
}