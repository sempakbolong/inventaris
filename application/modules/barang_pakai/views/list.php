<?php echo $this->session->flashdata('alert')?>
<div class="box box-default">
	<div class="box-body">		
		<a href="<?php echo site_url($module.'/add'.get_query_string()) ?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</a>
	</div>
</div>			
<div class="box box-default">
	<div class="box-header">
		<form action="<?php echo site_url($module.'/search'.get_query_string(null,'offset')) ?>" class="form-inline" method="post">
			<div class="form-group">
				<label for="limit">Limit :</label>
				<?php echo form_dropdown('limit',array('10'=>'10','50'=>'50','100'=>'100'),set_value('limit',$this->input->get('limit')),'onchange="submit()" class="form-control input-sm"')?> 
			</div>
			<div class="form-group">
				<?php echo form_input(array('name'=>'label','value'=>$this->input->get('label'),'autocomplete'=>'off','placeholder'=>'Label..','onchange=>"submit()"','class'=>'form-control input-sm'))?>
			</div>
			<div class="form-group">
				<?php echo form_dropdown('barang',$this->general_model->dropdown('barang','Nama Barang'),$this->input->get('barang'),'class="form-control input-sm select2" onchange="submit()"')?>
			</div>
			<div class="form-group">
				<?php echo form_dropdown('status',$this->general_model->dropdown('barang_pakai_status','Status'),$this->input->get('status'),'class="form-control input-sm" onchange="submit()"')?>
			</div>
			<div class="form-group">
				<label for="search">Tanggal :</label>
				<?php echo form_input(array('name'=>'date_from','value'=>$this->input->get('date_from'),'autocomplete'=>'off','placeholder'=>'Dari','size'=>'10','class'=>'form-control input-sm input-tanggal'))?>
				<?php echo form_input(array('name'=>'date_to','value'=>$this->input->get('date_to'),'autocomplete'=>'off','placeholder'=>'Sampai','size'=>'10','class'=>'form-control input-sm input-tanggal'))?>
			</div>			
			<button class="btn btn-primary btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span> Filter</button>
		</form>
	</div>
	<div class="box-body">
		<form action="<?php echo site_url($module.'/delete'.get_query_string()) ?>" class="form-check-delete" method="post">
			<div class="table-responsive">
				<?php echo $table; ?>
			</div>
		</form>
	</div>
</div>
<div class="box box-default">
	<div class="box-body">
		<label class="label-footer"><?php echo $total; ?></label>
		<div class="pull-right">
			<?php echo $pagination; ?>
		</div>
	</div>		
</div>
<div class="box box-default">
	<div class="box-body">
		<button id="delete-btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Hapus yang diceklis</button>
		<a href="<?php echo site_url('barang_pakai/pdf_empty_label') ?>" target="_blank" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-print"></span> Cetak Label Kosong</a>
		<a href="<?php echo site_url('barang_pakai/pdf_label') ?>" target="_blank" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-print"></span> Cetak Label</a>
	</div>
</div>