<?php echo $this->session->flashdata('alert')?>
<form action="<?php echo site_url($action) ?>" method="post">
<div class="box box-default">
	<?php echo $owner; ?>
	<div class="box-body">
		<div class="form-group form-inline">
			<?php echo form_label('Label','label',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'label','class'=>'form-control input-sm input-label','size'=>'50','maxlength'=>'50','autocomplete'=>'off','readonly'=>'true','value'=>set_value('label',(isset($row->label)?$row->label:''))))?>
			<small><?php echo form_error('label')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal Beli','tanggal_beli',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal_beli','class'=>'form-control input-sm input-tanggal','size'=>'10','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('tanggal_beli',(isset($row->tanggal_beli)?format_dmy($row->tanggal_beli):''))))?>
			<small><?php echo form_error('tanggal_beli')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Barang','barang',array('class'=>'control-label'))?>
			<?php echo form_dropdown('barang',$this->general_model->dropdown('barang','Nama Barang'),set_value('barang',(isset($row->barang)?$row->barang:'')),'id="barang" class="form-control input-sm select2"')?>
			<small><?php echo form_error('barang')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Harga','harga',array('class'=>'control-label'))?>
			<?php echo form_input(array('id'=>'harga','name'=>'harga','class'=>'form-control input-sm input-uang','maxlength'=>'20','autocomplete'=>'off','value'=>set_value('harga',(isset($row->harga)?$row->harga:''))))?>
			<small><?php echo form_error('harga')?></small>
		</div>
	</div>
</div>
<div class="box box-default">
	<div class="box-body">		
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal Pakai','tanggal_pakai',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal_pakai','class'=>'form-control input-sm input-tanggal','size'=>'10','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('tanggal_pakai',(isset($row->tanggal_pakai)?format_dmy($row->tanggal_pakai):''))))?>
			<small><?php echo form_error('tanggal_pakai')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Lokasi','lokasi',array('class'=>'control-label'))?>
			<?php echo form_dropdown('lokasi',$this->general_model->dropdown('lokasi','Lokasi'),set_value('lokasi',(isset($row->lokasi)?$row->lokasi:'')),'id="lokasi" class="form-control input-sm select2"')?>
			<small><?php echo form_error('lokasi')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Pengguna','pengguna',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'pengguna','class'=>'form-control input-sm input-pengguna','size'=>'25','maxlength'=>'50','autocomplete'=>'off','value'=>set_value('pengguna',(isset($row->pengguna)?$row->pengguna:''))))?>
			<small><?php echo form_error('pengguna')?></small>
		</div>
	</div>
</div>
<div class="box box-default">
	<div class="box-body">				
		<div class="form-group form-inline">
			<?php echo form_label('Kondisi','kondisi',array('class'=>'control-label'))?>
			<?php echo form_dropdown('kondisi',$this->general_model->dropdown('kondisi','Kondisi'),set_value('kondisi',(isset($row->kondisi)?$row->kondisi:'1')),'id="kondisi" class="form-control input-sm select2"')?>
			<small><?php echo form_error('kondisi')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal Rusak','tanggal_rusak',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal_rusak','class'=>'form-control input-sm input-tanggal','size'=>'10','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('tanggal_rusak',(isset($row->tanggal_rusak)?format_dmy($row->tanggal_rusak):''))))?>
			<small><?php echo form_error('tanggal_rusak')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal Buang','tanggal_buang',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal_buang','class'=>'form-control input-sm input-tanggal','size'=>'10','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('tanggal_buang',(isset($row->tanggal_buang)?format_dmy($row->tanggal_buang):''))))?>
			<small><?php echo form_error('tanggal_buang')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Tanggal Jual','tanggal_jual',array('class'=>'control-label'))?>
			<?php echo form_input(array('name'=>'tanggal_jual','class'=>'form-control input-sm input-tanggal','size'=>'10','maxlength'=>'10','autocomplete'=>'off','value'=>set_value('tanggal_jual',(isset($row->tanggal_jual)?format_dmy($row->tanggal_jual):''))))?>
			<small><?php echo form_error('tanggal_jual')?></small>
		</div>
		<div class="form-group form-inline">
			<?php echo form_label('Status','status',array('class'=>'control-label'))?>
			<?php echo form_dropdown('status',$this->general_model->dropdown('barang_pakai_status','Status'),set_value('status',(isset($row->status)?$row->status:'1')),'id="status" class="form-control input-sm select2"')?>
			<small><?php echo form_error('status')?></small>
		</div>
	</div>
</div>
<div class="box box-default">	
	<div class="box-body">
		<button class="btn btn-success btn-sm" type="submit" onclick="return confirm('<?php echo $confirm ?>')"><span class="glyphicon glyphicon-save"></span> <?php echo $this->lang->line('save') ?></button>
		<a class="btn btn-danger btn-sm" href="<?php echo site_url($module.get_query_string()) ?>"><span class="glyphicon glyphicon-repeat"></span> <?php echo $this->lang->line('back') ?></a>		
	</div>
</div>
</form>