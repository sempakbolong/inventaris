<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_pakai_model extends CI_Model {

	private $tbl_name = 'barang_pakai';
	private $tbl_key = 'id';
	
	function query()
	{
		$data[] = $this->db->select(array(
			'a.*',
			'b.name as barang_name',
			'c.name as kondisi_name',
			'd.name as lokasi_name',
			'e.name as status_name',
		));
		$data[] = $this->db->from($this->tbl_name.' a');
		$data[] = $this->db->join('barang b','a.barang = b.id','left');
		$data[] = $this->db->join('kondisi c','a.kondisi = c.id','left');
		$data[] = $this->db->join('lokasi d','a.lokasi = d.id','left');
		$data[] = $this->db->join($this->tbl_name.'_status e','a.status = e.id','left');
		$label = $this->input->get('label');
		if($label <> ''){
			$data[] = $this->db->where('a.label',$label);
		}		
		$barang = $this->input->get('barang');
		if($barang <> ''){
			$data[] = $this->db->where('a.barang',$barang);
		}		
		$kondisi = $this->input->get('kondisi');
		if($kondisi <> ''){
			$data[] = $this->db->where('a.kondisi',$kondisi);
		}		
		$lokasi = $this->input->get('lokasi');
		if($lokasi <> ''){
			$data[] = $this->db->where('a.lokasi',$lokasi);
		}		
		$status = $this->input->get('status');
		if($status <> ''){
			$data[] = $this->db->where('a.status',$status);
		}		
		if($this->input->get('date_from') <> '' && $this->input->get('date_to') <> ''){			
			$data[] = $this->db->where('a.tanggal_pakai >=',format_ymd($this->input->get('date_from')));
			$data[] = $this->db->where('a.tanggal_pakai <=',format_ymd($this->input->get('date_to')));
		}		
		$data[] = $this->db->order_by($this->general->get_order_column('a.id'),$this->general->get_order_type('desc'));
		$data[] = $this->db->offset($this->general->get_offset());
		return $data;
	}
	function get()
	{
		$this->query();
		$this->db->limit($this->general->get_limit());
		return $this->db->get();
	}
	function get_all()
	{
		$this->query();
		return $this->db->get();
	}
	function report($date_from = '',$date_to = '')
	{
		$this->query();
		if($date_from <> '' && $date_to <> ''){			
			$this->db->where('a.tanggal_pakai >=',$date_from);
			$this->db->where('a.tanggal_pakai <=',$date_to);
		}		
		return $this->db->get();
	}
	function report_rusak($date_from = '',$date_to = '')
	{
		$this->query();
		$this->db->where('a.kondisi',2);
		if($date_from <> '' && $date_to <> ''){			
			$this->db->where('a.tanggal_rusak >=',$date_from);
			$this->db->where('a.tanggal_rusak <=',$date_to);
		}		
		return $this->db->get();
	}
	function report_stok()
	{
		$this->db->select(array(
			'b.code as barang_code',
			'b.name as barang_name',
			'c.name as jenis_name',
			'count(a.barang) as stok'
		));
		$this->db->from($this->tbl_name.' a');
		$this->db->join('barang b','a.barang=b.id','left');
		$this->db->join('barang_jenis c','b.jenis=c.id','left');
		$this->db->group_by('a.barang');
		$this->db->where('a.kondisi',1);
		$this->db->where('a.status',2);
		return $this->db->get();
	}
	function count_all()
	{
		$this->query();
		return $this->db->get()->num_rows();
	}
	function label($barang = '')
	{
		$this->db->from($this->tbl_name);
		$this->db->where('barang',$barang);
		$this->db->order_by('label','desc');
		$this->db->limit(1);
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			$label = explode('/', $result->row()->label);
			return (int)$label[0]+1;
		}else{
			return '1';
		}
	}
	function total($field,$id)
	{
		$this->db->from($this->tbl_name);
		$this->db->where($field,$id);
		return $this->db->get()->num_rows();
	}
}