<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['app_name'] = 'ADirect Inventory';
$config['app_alias'] = 'AI';
$config['theme'] = 'skin-red-light';